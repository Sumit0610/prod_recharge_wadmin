<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;
use Carbon\Carbon;
use App\User;
use App\AboutUser;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services;
use App\usercontact;
use App\Gallary;
use App\About_team;
use App\Slider;
use App\Map;
use App\GallaryVideos;
use App\ContactusModel;
use App\Vision_Mission;
use App\SskWork;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('new_home');
    }


    public function addlogo()
    {
        // return view('admin.logo');
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $logo = User::where('id',$id)->first();
                
     return view('logo')->with('logo',$logo);

    }

    public function logoadd(Request $req)
    {
        $image=$req->only('logo_img');
        $file = $image['logo_img'];
        if(isset($file))
        {
            $image_name= $file->getClientOriginalName();
            $extension = File::extension($image_name);
            $path = public_path().'//uploads//logo';
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $File_name=$timestamp . time() . '.' . $extension;
            $img1 = $req->file('logo_img')->getRealPath();
            $img = Image::make($img1);
            $img->resize(200,200,function($constraint){
                 $constraint->aspectRatio();
              })->save($path.'/'.$File_name);
        }else{
            return redirect() ->back()->with('status', 'Please select the Logo image' );
        }

        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

        $userdata = User::where('id',$id)->update(['logo'=>$File_name]);
        // $data = session()->all();
        // dd($data);
        return redirect() ->back()->with('status', 'Logo Updated Successfully' );
    }

    public function aboutus(){
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $cannotadd = false;
        $team = About_team::where('u_id',$id)->count();
        $about1 = AboutUser::where('user_id',$id)->first();
        $remaining = abs(12-$team);
        
        if($team!=0)
                {
                    
                    $count = $team; 
                    $remaining = abs(12-$team); 
                    if($count >= 12)
                    {
                        $cannotadd = true;
                    }
                    else
                    {
                        $cannotadd = false;
                        
                    }
                }
                if(isset($about1))
                {
                    
                    return view("aboutus")->with('cannotadd',$cannotadd)->with('remaining',$remaining)->with('about1',$about1);
                }else
                {
                    
                    return view("aboutus")->with('cannotadd',$cannotadd)->with('remaining',$remaining)->with('about1',new AboutUser);
                }

    }

    public function aboutus_save(Request $req){
        $i=1;
            $image=$req->only('teammemimg');
            $file = $image['teammemimg'];
            $ftype = array();
            $attributeNames = array(
           'teammemnm.*' => 'Team member name',
           'teammempos.*' => 'Team member position',
           'teammemimg'            => 'Team member image',
           'teammemimg.*' => 'Team member image'     
        );
           

            $validator = Validator::make($req->all(), [
                'teammemimg'            => 'required',
                'teammemimg.*'            => 'required|image|mimes:jpeg,jpg,png',
                'teammemnm'            => 'required|min:1|max:5',    
                'teammempos'         => 'required|min:1|max:5',
                'teammemnm.*'            => 'required|min:2',    
                'teammempos.*'         => 'required|min:2'
                ]);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_us')
                    ->withErrors($validator);

            }else {
               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];           

                $team1 = About_team::where('u_id',$id)->count();

                //echo $team1;
                if(isset($team1))
                {
                    $count = $team1 + sizeof(Input::get('teammemnm')); 
                    $remaining = abs(5-$team1); 
                    if($count > 5)
                    {
                        $validator->errors()->add('teammemnm', 'You exceded the upper limit of team members. You already added '. $team1 .' team members. You can add at most '. $remaining .' team members');
                        //$validator->errors()->add('teammemnm', 'You exceded the upper limit of team members. You can add at most 5 team members '.$team1.' '.sizeof(Input::get('teammemnm')));
                        // $validator->addMessage("Team size Upper limit has been reached");  error Method [addMessage] does not exist.
                        return Redirect::to('/admin/about_us')
                          ->withErrors($validator);     
                    }else
                    {
                                
                                if(isset($file)){
                foreach ($file as $key => $value) {
                    # code...

                    $image_name= $value->getClientOriginalName();
                    
                   
                    if(isset($value))
                    {
                        $image_name= $value->getClientOriginalName();
                        $extension = File::extension($image_name);
                        $path = public_path().'//uploads//team';

                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $File_name=$timestamp . (time()+$i) . '.' . $extension;
                        $img1 = $value->getRealPath();
                        
                        $img = Image::make($img1);
                        $img->resize(200,200,function($constraint){
                             $constraint->aspectRatio();
                          })->save($path.'/'.$File_name);

                        //array_push($ftype,$File_name);
                        $ftype[$key]=$File_name;
                    }else{
                        return redirect() ->back()->with('status', 'Please select the team member image' );
                    }
                    $i++;
                }
                
                }
  
                    for ($i=0; $i < sizeof(Input::get('teammemnm')); $i++) { 
                            # code...
                            $team = new About_team;
                            $team->team_member_name = (String)Input::get('teammemnm')[$i];
                            $team->team_member_pos = (String)Input::get('teammempos')[$i];
                            if(isset($ftype[$i])){
                            $team->team_member_img = $ftype[$i];
                        }
                            $team->u_id = $id;
                            $team->save();

                        }
                    }
                
                }
                else
                {
                        if(isset($file)){
            foreach ($file as $key => $value) {
                # code...

                $image_name= $value->getClientOriginalName();
                
               
                if(isset($value))
                {
                    $image_name= $value->getClientOriginalName();
                    $extension = File::extension($image_name);
                    $path = public_path().'\uploads\team\\';
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $File_name=$timestamp . (time()+$i) . '.' . $extension;
                    $img1 = $value->getRealPath();
                    
                    $img = Image::make($img1);
                    $img->resize(200,200,function($constraint){
                         $constraint->aspectRatio();
                      })->save($path.'/'.$File_name);

                    //array_push($ftype,$File_name);
                    $ftype[$key]=$File_name;
                }else{
                    return redirect() ->back()->with('status', 'Please select the team member image' );
                }
                $i++;
            }
            
            }
                        
                    for ($i=0; $i < sizeof(Input::get('teammemnm')); $i++) { 
                            # code...
                            $team = new About_team;
                            $team->team_member_name = (String)Input::get('teammemnm')[$i];
                            $team->team_member_pos = (String)Input::get('teammempos')[$i];
                            if(isset($ftype[$i])){
                            $team->team_member_img = $ftype[$i];
                        }
                            $team->u_id = $id;
                            $team->save();

                        }
                }
                

               return redirect()->back()->with('status', 'About details has been Saved' );
                

            }
            
        

    }


     public function services(){
        return view("services");
    }

    public function addservices(Request $req)
    {
        $attributeNames = array(
           'servicename.*' => 'Service name',
           'servicedesc.*' => 'Service Description',     
        );
            $rules = array(
                'servicename'             => 'required|min:1',
                'servicedesc'            => 'required|min:1',
                'servicename.*'         => 'required|min:2', 
                'servicedesc.*'         => 'required|min:2'
                
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/services')
                    ->withErrors($validator);

            } else {
               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                
                for ($i=0; $i < sizeof(Input::get('servicename')); $i++) { 
                    # code...
                    $service = new Services;
                    $service->servicename = (String)Input::get('servicename')[$i];
                    $service->servicedesc = (String)Input::get('servicedesc')[$i];
                    $service->u_id = $id;
                    $service->save();

                }
            
                
                return redirect()->back()->with('status', 'Services are saved Successfully' );


            }
    }

     public function gallery(){
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
             $gallary = Gallary::Where('u_id',$id)->first();
             if(isset($gallary))
             {
                return view("gallery")->with('gallary',$gallary);
             }
             else{
                return view("gallery")->with('gallary',new Gallary);
             }
    }

     public function contact_us(){
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                    
                    $contact = usercontact::Where('u_id',$id)->first();
                    $contact1_count = usercontact::Where('u_id',$id)->count();                    
                    if($contact1_count > 0)
                    {
                        //echo $contact;
                        return view("contact_us")->with('contact',$contact);
                    }
                    else{
                        return view("contact_us")->with('contact',new usercontact);
                    }
        
    }

    public function save_contact_us(Request $req)
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                    $contact1_count = usercontact::Where('u_id',$id)->count();
                    echo $contact1_count;
        $attributeNames = array(
           'contactmail' => 'Contact Mail',
           'contactnumber' => 'Contact Number', 
           'contactaddress' => 'Contact address',
           'facebooklink' => 'Facebook link',
           'twitterlink' => 'Twitter link',
           'linkedinlink' => 'Linkedin Link',
           'instagramlink' => 'Instagram link',
           'googlepluslink' => 'Google plus link',
           'youtubelink' => 'Youtube link',    
        );
        if(isset($contact1_count))
        {
            $rules = array(
                'contactmail'             => 'nullable|email',
                'contactnumber'            => 'nullable|digits:10',
                'contactaddress'         => 'nullable|min:10',
                'facebooklink'         => 'nullable|min:2',
                'twitterlink'         => 'nullable|min:2',
                'linkedinlink'         => 'nullable|min:2',
                'instagramlink'         => 'nullable|min:2',
                'googlepluslink'         => 'nullable|min:2',
                'youtubelink'         => 'nullable|min:2',
            );
        }else{
            $rules = array(
                'contactmail'             => 'required|email',
                'contactnumber'            => 'required|digits:10',
                'contactaddress'         => 'required|min:10',
                'facebooklink'         => 'nullable|min:2',
                'twitterlink'         => 'nullable|min:2',
                'linkedinlink'         => 'nullable|min:2',
                'instagramlink'         => 'nullable|min:2',
                'googlepluslink'         => 'nullable|min:2',
                'youtubelink'         => 'nullable|min:2',
            );
        }

            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/contact_us')
                    ->withErrors($validator);

            } else {
               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                    
                    $contact1 = usercontact::Where('u_id',$id)->first();
                    if(isset($contact1))
                    {
                        if ($req->has('contactmail')) {
                            $contact1->umail = Input::get('contactmail'); 
                        }  
                        if ($req->has('contactnumber')) {
                            $contact1->umobile = Input::get('contactnumber'); 
                        }  
                        if ($req->has('contactaddress')) {
                            $contact1->uaddress = Input::get('contactaddress'); 
                        }  
                        if ($req->has('facebooklink')) {
                            $contact1->facebook = Input::get('facebooklink'); 
                        }  
                        if ($req->has('twitterlink')) {
                            $contact1->twitter = Input::get('twitterlink'); 
                        }  
                        if ($req->has('linkedinlink')) {
                            $contact1->linkedin = Input::get('linkedinlink'); 
                        }  
                        if ($req->has('instagramlink')) {
                            $contact1->instagram = Input::get('instagramlink'); 
                        }  
                        if ($req->has('googlepluslink')) {
                            $contact1->googleplus = Input::get('googlepluslink'); 
                        }  
                        if ($req->has('youtubelink')) {
                            $contact1->youtube = Input::get('youtubelink'); 
                        }
                        $contact1->u_id = $id;
                        $contact1->save();
                    }else{
                        $contact = new usercontact;
                        if ($req->has('contactmail')) {
                            $contact->umail = Input::get('contactmail'); 
                        }  
                        if ($req->has('contactnumber')) {
                            $contact->umobile = Input::get('contactnumber'); 
                        }  
                        if ($req->has('contactaddress')) {
                            $contact->uaddress = Input::get('contactaddress'); 
                        }  
                        if ($req->has('facebooklink')) {
                            $contact->facebook = Input::get('facebooklink'); 
                        }  
                        if ($req->has('twitterlink')) {
                            $contact->twitter = Input::get('twitterlink'); 
                        }  
                        if ($req->has('linkedinlink')) {
                            $contact->linkedin = Input::get('linkedinlink'); 
                        }  
                        if ($req->has('instagramlink')) {
                            $contact->instagram = Input::get('instagramlink'); 
                        }  
                        if ($req->has('googlepluslink')) {
                            $contact->googleplus = Input::get('googlepluslink'); 
                        }  
                        if ($req->has('youtubelink')) {
                            $contact->youtube = Input::get('youtubelink'); 
                        }
                    
                        $contact->u_id = $id;
                        $contact->save();
                    }
                   

                return redirect()->back()->with('status', 'Contact Details Updated Successfully' );

            }
    }

    public function aboutlist()
    {
        $data = session()->all();
        $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $aboutlst = AboutUser::where('user_id',$id)->first();
        $team1 = About_team::where('u_id',$id)->get();
        if(isset($aboutlst) && isset($team1))
        {
            return view('aboutlist')->with('about1',$aboutlst)->with('team',$team1);
        }
        if(isset($aboutlst))
        {
            return view('aboutlist')->with('about1',$aboutlst)->with('team',new About_team);
        }
        if(isset($team1))
        {
            return view('aboutlist')->with('about1',new AboutUser)->with('team',$team1);
        }
        return view('aboutlist')->with('about1',new AboutUser)->with('team',new About_team);
    }




    public function aboutlist_save(Request $req)
    {
        $rules = array(
                'aboutusdesc'             => 'required|min:20' 
            );


            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_list')
                    ->withErrors($validator);

            } else {
               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $about = AboutUser::where('user_id',$id)->first();
                $about->about_desc     = Input::get('aboutusdesc');
               
                $about->user_id = $id;
                
                $about->save();

                
                return redirect()->back()->with('status', 'About us Details Updated Successfully' );
            }
        
    }

    public function aboutteam_save(Request $req)
    {
        $rules = array(
                'teammemnm'             => 'nullable|min:2',
                'teammempos'             => 'nullable|min:2' 
            );


            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_list')
                    ->withErrors($validator);

            } else {
                    $File_name;
                    $image=$req->only('temimg');
                    $file = $image['temimg'];
                     
                    if(isset($file))
                    {
                        $image_name= $file->getClientOriginalName();
                        $extension = File::extension($image_name);
                        $path = public_path().'//uploads//team';
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $File_name=$timestamp . time() . '.' . $extension;
                        $img1 = $req->file('temimg')->getRealPath();
                        $img = Image::make($img1);
                        $img->resize(200,200,function($constraint){
                             $constraint->aspectRatio();
                          })->save($path.'/'.$File_name);
                    }

               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $about = About_team::where('team_id',Input::get('teamid'))->first();
                $about->team_member_name     = Input::get('teammemnm');
                $about->team_member_pos     = Input::get('teammempos');
                if(isset($file)){
                $about->team_member_img     = $File_name;}
                $about->save();

                
                return redirect()->back()->with('status', 'About us Details Updated Successfully' );
            }
        
    }



    public function serviceslist()
    {
        $data = session()->all();
        $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $servicelst = Services::where('u_id',$id)->get();
        return view('servicelist')->with('servicelst',$servicelst);
    }

    public function service_list(Request $req)
    {
        $attributeNames = array(
           'servicename' => 'Service name',
           'servicedesc' => 'Service Description',     
        );
            $rules = array(
                'servicename'             => 'required|min:1',
                'servicedesc'            => 'required|min:1',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/service_list')
                    ->withErrors($validator);

            } else {   
                   $id = Input::get('sid');
                     $service = Services::where('id',$id)->first();
                    $service->servicename = Input::get('servicename');
                    $service->servicedesc = Input::get('servicedesc');
                    $service->save();
               //       var_dump($service);
               // echo Input::get('sid');
            
                
                return redirect()->back()->with('status', 'Services are saved Successfully' );

            }
    }

    public function contact_list()
    {
        $data = session()->all();
        $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $contactlst = usercontact::where('u_id',$id)->first();
        if(isset($contactlst))
        {
            return view('contact_list')->with('contactlst',$contactlst);
        }
        else{
            return view('contact_list')->with('contactlst',new usercontact);
        }
        
        
    }

    public function update_contact_list(Request $req)
    {
        //var_dump($_POST);
        $attributeNames = array(
           'contactmail' => 'Contact Mail',
           'contactnumber' => 'Contact Number', 
           'contactaddress' => 'Contact address',
           'facebooklink' => 'Facebook link',
           'twitterlink' => 'Twitter link',
           'linkedinlink' => 'Linkedin Link',
           'instagramlink' => 'Instagram link',
           'googlepluslink' => 'Google plus link',
           'youtubelink' => 'Youtube link',    
        );
            $rules = array(
                'contactmail'             => 'nullable|email',
                'contactnumber'            => 'nullable|digits:10',
                'contactaddress'         => 'nullable|min:10', 
                'facebooklink'         => 'nullable|min:2',
                'twitterlink'         => 'nullable|min:2',
                'linkedinlink'         => 'nullable|min:2',
                'instagramlink'         => 'nullable|min:2',
                'googlepluslink'         => 'nullable|min:2',
                'youtubelink'         => 'nullable|min:2',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                // return Redirect::to('/admin/contact_list')
                //     ->withErrors($validator);

            } else {
               $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                
                
                    $contact = usercontact::where('u_id',$id)->first();
                    $contact->umail = Input::get('contactmail');
                    $contact->umobile = Input::get('contactnumber');
                    $contact->uaddress = Input::get('contactaddress');
                    $contact->facebook = Input::get('facebooklink');
                    $contact->twitter = Input::get('twitterlink');
                    $contact->linkedin = Input::get('linkedinlink');
                    $contact->instagram = Input::get('instagramlink');
                    $contact->googleplus = Input::get('googlepluslink');
                    $contact->youtube = Input::get('youtubelink');
                    $contact->u_id = $id;
                    $contact->save();

                return redirect()->back()->with('status', 'Contact Details Updated Successfully' );

            }
    }

    public function index_s(){
       
    }

    public function about_delete()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $about = AboutUser::where('user_id',$id)->first();
        $team = About_team::where('u_id',$id)->get();
        if(isset($about) && isset($team))
        {
            return view('delete_about')->with('about',$about)->with('team',$team);
        }
        else if(isset($about)){
            return view('delete_about')->with('about',$about)->with('team',new About_team);
        }
        else if(isset($team)){
            return view('delete_about')->with('about',new AboutUser)->with('team',$team);
        }
        else{
            return view('delete_about')->with('about',new AboutUser)->with('team',new About_team);
        }
        
    }

    
    public function aboutdesc_save(Request $req)
    {

        $attributeNames = array(
                'aboutusdesc'             => 'About Description'
            );
         $rules = array(
                'aboutusdesc'             => 'required|min:20'
            );
      
        $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_us')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $about1 = new AboutUser;
                $about1->about_desc = Input::get('aboutusdesc');
                $about1->user_id = $id;
                $about1->save();
            }

            return redirect()->back()->with('status', 'About Description data has been saved successfully');

    }

    public function aboutdesc_delete(Request $req)
    {
        $id = $req->aid;
        $about = AboutUser::find($id);
        $about->delete();
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        // $service = Services::where('u_id',$id)->first();
        // $contact = usercontact::where('u_id',$id)->first();
        
        // if(!isset($service) && !isset($contact))
        // {
            
        // }
        $data = User::find($id);
            $data->publish='no';
            $data->save();
            return redirect()->back()->with('status', 'About Description has been deleted and your site gets unpublished' );
    
    }

    public function abouttem_delete(Request $req)
    {
        $id = $req->aid;
        $team = About_team::find($id);
        $team->delete();

        return redirect()->back()->with('status', 'Team member details has been deleted' );
    }

   

    public function delservice_list()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $service = Services::where('u_id',$id)->get();
        return view('delservice_list')->with('service',$service);
    }


    public function delservice(Request $req)
    {
        $id = $req->sid;
        $service = Services::find($id);
        $service->delete();

        $data = session()->all();
                $uid =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $service_count = Services::where('u_id',$uid)->count();
        var_dump($service_count);
        if($service_count >0)
        {
            return redirect()->back()->with('status', 'Service has been deleted');
        }else{
            $data = session()->all();
                $uid =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
            $data = User::find($uid);
            $data->publish='no';
            $data->save();
            return redirect()->back()->with('status', 'All Services has been deleted and your site gets unpublished');
        }
        
        
    }
    public function del_contactlist()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $contact = usercontact::where('u_id',$id)->first();
        return view('delcontact_list')->with('contact',$contact);
    }

    public function delcontactmail(Request $req)
    {
        $id = $req->cid;
        $about = usercontact::find($id);
        $about->umail = null;
        $about->save();

        return redirect()->back()->with('status', 'Contact Mail has been deleted' );
    }

    public function savegallery(Request $req)
    {
        $image=$req->only('logo_img');
        $file = $image['logo_img'];
         $image1=$req->only('image_2');
        $file1 = $image1['image_2'];
        if(isset($file))
        {
            $image_name= $file->getClientOriginalName();
            $extension = File::extension($image_name);
            $path = public_path().'//uploads//gallery';
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $File_name=$timestamp . time() . '.' . $extension;
            $img1 = $req->file('logo_img')->getRealPath();
            $img = Image::make($img1);
            $img->resize(400,400,function($constraint){
                 $constraint->aspectRatio();
              })->save($path.'/'.$File_name);
        }


        if(isset($file1))
        {
            $image_name1= $file1->getClientOriginalName();
            $extension1 = File::extension($image_name1);
            $path1 = public_path().'//uploads//gallery';
            $timestamp1 = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $File_name1=$timestamp1 . (time()+100) . '.' . $extension1;
            $img2 = $req->file('image_2')->getRealPath();
            $img3 = Image::make($img2);
            $img3->resize(400,400,function($constraint){
                 $constraint->aspectRatio();
              })->save($path1.'/'.$File_name1);
        }

        
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $gallary1 = Gallary::where('u_id',$id)->first();
        var_dump($gallary1);
        if(isset($gallary1))
        {   if(isset($file))
            {
                $gallary1->galleryimg1 = $File_name;
            }
            if(isset($file1))
            {
                $gallary1->galleryimg2 = $File_name1;
            }
            $gallary1->save();
        }
        else{
            $gallary = new Gallary;
            if(isset($file))
            {
                $gallary->galleryimg1 = $File_name;
            }
            if(isset($file1))
            {
                $gallary->galleryimg2 = $File_name1;
            }
            $gallary->u_id = $id;
            $gallary->save();
        }
        
        
        return redirect() ->back()->with('status', 'Gallery Updated Successfully' );
    }

    public function saveslider(Request $req)
    {
        $image=$req->only('image_1');
        $file = $image['image_1'];
         $image1=$req->only('image_2');
        $file1 = $image1['image_2'];
        if(isset($file))
        {
            $image_name= $file->getClientOriginalName();
            $extension = File::extension($image_name);
            $path =  public_path().'//uploads//slider';

            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $File_name=$timestamp . time() . '.' . $extension;
            $img1 = $req->file('image_1')->getRealPath();
            $img = Image::make($img1);
            $img->resize(1000,700,function($constraint){
                 $constraint->aspectRatio();
              })->save($path.'/'.$File_name);
        }


        if(isset($file1))
        {
            $image_name1= $file1->getClientOriginalName();
            $extension1 = File::extension($image_name1);
            $path1 =  public_path().'//uploads//slider';
            $timestamp1 = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $File_name1=$timestamp1 . (time()+100) . '.' . $extension1;
            $img2 = $req->file('image_2')->getRealPath();
            $img3 = Image::make($img2);
            $img3->resize(1000,700,function($constraint){
                 $constraint->aspectRatio();
              })->save($path1.'/'.$File_name1);
        }

        
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $slider1 = Slider::where('u_id',$id)->first();
        
        if(isset($slider1))
        {
            if(isset($file)){
                $slider1->slider_img1 = $File_name;
            }
            if(isset($file1)){
                $slider1->slider_img2 = $File_name1;
            }
            $slider1->save();
        }
        else{
            $slider = new Slider;
            if(isset($file)){
                $slider->slider_img1 = $File_name;
            }
            if(isset($file1)){
                $slider->slider_img2 = $File_name1;
            }
            
            $slider->u_id = $id;
            $slider->save();
        }
        
        
        return redirect() ->back()->with('status', 'Slider Updated Successfully' );
    }



    public function weburl(){
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d']; 

        $data_website = User::where('id', $id)->first();  
    
            return view('web_url_expected')->with('data_website', $data_website);
        
    }

    public function weburl_store(Request $req){

        $website_link = Input::get("website_link");
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

        //$gallary1 = User::where('id',$id)->update(['website_link' => $website_link]);

        $gallary = User::where('website_link',$website_link)->first();

        if(isset($gallary))
        {
            return redirect()->back()->with('status', 'Website URL is already in use. Please select some other Website URL.');
        }else{
            $gallary1 = User::where('id',$id)->update(['website_link' => $website_link]);
        }
        return redirect()->back()->with('status', 'Website URL udpated successfully!');
    }

    public function addslider()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $slider1 = Slider::where('u_id',$id)->first();
        if($slider1)
        {
            return view('slider')->with('slider',$slider1);
        }
        else{
             return view('slider')->with('slider',new Slider);
        }
        
    }

    public function addmap()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $map1 = Map::where('u_id',$id)->first();
                $mapAdd='Add';
                $mapUpdate='Update';
                //var_dump($map1);
                if(isset($map1))
                {
                    return view('addmap')->with('map',$mapUpdate)->with('mymap',$map1);
                    
                }else{
                    return view('addmap')->with('map',$mapAdd);
                }
        
    }


    public function savemap(Request $req)
    {
        $attributeNames = array(
                'lat'             => 'Latitude',
                'lng'             => 'Longitude'
            );
         $rules = array(
                'lat'             => 'required',
                'lng'             => 'required'
            );
        $lat = Input::get('lat');
        $lng = Input::get('lng');
        $autocomplete = Input::get('autocomplete');
        $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/map')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $map1 = Map::where('u_id',$id)->first();
                if($map1)
                {
                    $map1->longitude = $lat;
                    $map1->latitude = $lng;
                    $map1->map_loc = $autocomplete;
                    $map1->save();
                }
                else {
                $map2 = new Map;
                $map2->longitude = $lat;
                $map2->latitude = $lng;
                $map2->map_loc = $autocomplete;
                $map2->u_id = $id;
                $map2->save();
                }
            }

            return redirect()->back()->with('status', 'Your map has been added Suceessfully');

    }

    public function gal_videos()
    {
        return view('gal_videos');
    }

    public function savegal_videos(Request $req)
    {
            $attributeNames = array(
           'gallery_videos' => 'Gallery videos',
           'gallery_videos.*'   => 'Gallery video link'
        );
            $rules = array(
                'gallery_videos'             => 'required|min:1',
                'gallery_videos.*'             => 'required|min:2'
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/gal_videos')
                    ->withErrors($validator);

            } else {



            $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                


                for ($i=0; $i < sizeof(Input::get('gallery_videos')); $i++) { 
                    # code...
                    $vid = new GallaryVideos;
                    $vid->gallary_video_link = Input::get('gallery_videos')[$i];
                    $vid->u_id = $id;
                    $vid->save();

                }
            
        }

        return redirect()->back()->with('status', 'Your Gallery videos added Suceessfully');

    }


    public function gal_editvideos()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $gal = GallaryVideos::Where('u_id',$id)->get();
        if(isset($gal))
        {
             return view('gal_editvideos')->with('gal',$gal);
        }else{
            return view('gal_editvideos')->with('gal',new GallaryVideos);
        }
    }

    public function gal_savevideos(Request $req)
    {
         $attributeNames = array(
           'gallery_videos' => 'Gallery video link'
        );
            $rules = array(
                'gallery_videos'             => 'required|min:2',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/gal_editvideos')
                    ->withErrors($validator);

            } else {
            
                $id=Input::get('gal_videos');
                 $vid = GallaryVideos::Where('galvid_id',$id)->first();
                    $vid->gallary_video_link = Input::get('gallery_videos');
                    
                    $vid->save();

            }

            return redirect()->back()->with('status', 'Your Gallery videos saved Suceessfully');
    }

    public function gal_deletevideos()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $gal = GallaryVideos::Where('u_id',$id)->get();
        if(isset($gal))
        {
             return view('gal_deletevideos')->with('gal',$gal);
        }else{
            return view('gal_deletevideos')->with('gal',new GallaryVideos);
        }
    }

    public function gal_delvideos(Request $req)
    {

        $attributeNames = array(
           'gal_videos' => 'Gallery video link'
        );
            $rules = array(
                'gal_videos'             => 'required|min:1',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/gal_deletevideos')
                    ->withErrors($validator);

            } else {
                $id = Input::get('gal_videos');
                $vid = GallaryVideos::Where('galvid_id',$id)->first();
                $vid->delete();
            }
            return redirect()->back()->with('status', 'Your Gallery video has been deleted Suceessfully');
    }

    public function template()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $vid = User::Where('id',$id)->first();
        $vtem = $vid->template;
        $vweb = $vid->website_link;
        $vcat = $vid->category;
        if(isset($vtem))
        {
            return view('template')->with('vtem',$vtem)->with('vweb',$vweb)->with('vcat',$vcat);
        }
        return view('template');
    }

    public function save_template(Request $req)
    {
        $attributeNames = array(
           'templateoption' => 'Template Option'
        );
            $rules = array(
                'templateoption'             => 'required',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/template')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = User::Where('id',$id)->first();
                $vid->template = Input::get('templateoption');
                $vid->save();
            }

            return redirect()->back()->with('status', 'Your template option  saved Suceessfully');
    }

    public function publish()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = User::Where('id',$id)->first();
                $vpub = $vid->publish;
                 $vabout = AboutUser::Where('user_id',$id)->first();
                 $vservices = Services::Where('u_id',$id)->first();
                 $vcontact = usercontact::Where('u_id',$id)->first();
        if(isset($vabout) && isset($vservices) && isset($vcontact))
        {
            if(isset($vpub))
            {
                return view('publish')->with('vpub',$vpub)->with('allset',true);
            }
            else{
                return view('publish')->with('vpub','no')->with('allset',true);
            }
        }
        else{
            return view('publish')->with('vpub','')->with('allset',false);
        }
        
    }

    public function save_publish(Request $req)
    {
        $attributeNames = array(
           'publishoption' => 'Publish Option'
        );
            $rules = array(
                'publishoption'             => 'required',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/publish')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = User::Where('id',$id)->first();
                $vid->publish = Input::get('publishoption');
                $vid->save();
            }
            if(Input::get('publishoption')=='yes'){
                return redirect()->back()->with('status', 'You published your website Suceessfully');
            }else{
                return redirect()->back()->with('status', 'You unpublished your website Suceessfully');
            }
            
    }

    public function profile_page(){
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $user_data = User::Where('id',$id)->first();

                return view('profile_page')->with('user_data', $user_data);
    }


    public function current_template(){
         $data = session()->all();
                $query =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $data_website = User::where('id', $query)->first();
        $data_website_count = User::where('id', $query)->count();
        $cat = $data_website->category;
        if($data_website_count > 0) {

                $data_services = Services::where('u_id', $data_website->id)->get();
                $data_sliders = Slider::where('u_id', $data_website->id)->get();
                $data_contactus = usercontact::where('u_id', $data_website->id)->first();
                $data_about = AboutUser::where('user_id', $data_website->id)->first();
                $data_team = About_team::where('u_id', $data_website->id)->get();
                $data_location = Map::where('u_id', $data_website->id)->first();
                $data_videos = GallaryVideos::where('u_id', $data_website->id)->get();

                return view('website.'.$data_website->template)->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos);
            
        }
        else {
            return view('error_page');
        }
    }

     public function contactusdata($w, $t, $q){

        $data_website = User::where('id', $q)->first();
        $data_contactus = usercontact::where('u_id', $q)->first();
        $data_location = Map::where('u_id', $q)->first();

             return view('website.contact_unpublished')->with('data_contactus', $data_contactus)->with('data_website', $data_website)->with('data_location', $data_location);

    }

    public function aboutdata($w, $t, $q){

        $data_website = User::where('id', $q)->first();
        $data_about = AboutUser::where('user_id', $q)->first();
        $data_team = About_team::where('u_id', $q)->get();

            return view('website.about_unpublished')->with('data_about', $data_about)->with('data_website', $data_website)->with('data_team', $data_team);
    
    }

    public function gallerydata($w, $t, $q){

        $data_website = User::where('id', $q)->first();
        $data_gallery = Gallary::where('u_id', $q)->first();
        $data_videos = GallaryVideos::where('u_id', $q)->get();

            return view('website.gallery_unpublished')->with('data_gallery', $data_gallery)->with('data_website', $data_website)->with('data_videos', $data_videos); 
 
    }

    public function demo_website($query, $demo_website){

        if($query == 'info.com'){

            $data_website = User::where('website_link', $query)->first();
            $data_website_count = User::where('website_link', $query)->count();

            if($data_website_count > 0) {

            $data_services = Services::where('u_id', $data_website->id)->get();
            $data_sliders = Slider::where('u_id', $data_website->id)->get();
            $data_contactus = usercontact::where('u_id', $data_website->id)->first();
            $data_about = AboutUser::where('user_id', $data_website->id)->first();
            $data_team = About_team::where('u_id', $data_website->id)->get();
            $data_location = Map::where('u_id', $data_website->id)->first();
            $data_videos = GallaryVideos::where('u_id', $data_website->id)->get();
            $data_gallery = Gallary::where('u_id', $data_website->id)->first();

            if ($demo_website == 2) {
                return view('website.template2')->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos)->with('data_gallery', $data_gallery);
            }
            else if ($demo_website == 3) {
                return view('website.template3')->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos)->with('data_gallery', $data_gallery);
            }
            else if ($demo_website == 4) {
                return view('website.template4')->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos)->with('data_gallery', $data_gallery);
            }
            else if ($demo_website == 5) {
                return view('website.index_t5')->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos)->with('data_gallery', $data_gallery);
            }
            else if($demo_website == 6){
                return view('website.template6')->with('data_services', $data_services)->with('data_website', $data_website)->with('data_sliders', $data_sliders)->with('data_contactus', $data_contactus)->with('data_about', $data_about)->with('data_team', $data_team)->with('data_location', $data_location)->with('data_videos', $data_videos)->with('data_gallery', $data_gallery);
            }
            else {
                return view('hit_wrong_links');
            }

            }
            else {
                return view('hit_wrong_links');
            }
        }
        else{
                return view('hit_wrong_links');
        }    
    }


    public function queries(){
        $data = session()->all();
        $query =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $data_contactus = ContactusModel::where('u_id', $query)->orderBy("id", "desc")->get();
        return view('queries_list')->with('data_contactus', $data_contactus);
    }



    public function add_vmission()
    {$data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = Vision_Mission::where('ssk_uid',$id)->first();
                if(isset($vid))
                {
                    return view('add_vision_mission')->with('vm',$vid)->with('notset',true);
                }
        return view('add_vision_mission')->with('vm',new Vision_Mission)->with('notset',false);
    }

    public function save_vmission()
    {
        $attributeNames = array(
           'vision' => 'Vision',
           'mission' => 'Mission',
        );
            $rules = array(
                'vision'             => 'required|min:10',
                'mission'             => 'required|min:10',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/add_vmission')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = new Vision_Mission;
                $vid->vision = Input::get('vision');
                $vid->mission = Input::get('mission');
                $vid->ssk_uid = $id;
                $vid->save();
            }
            return redirect()->back()->with('status', 'Your vision and mission has been saved Suceessfully');
    }

    public function vmission_list()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = Vision_Mission::where('ssk_uid',$id)->first();
                if(isset($vid))
                {
                    return view('edit_vision_mission')->with('vm',$vid);
                }
        return view('edit_vision_mission')->with('vm',new Vision_Mission);
    }

    public function save_vmission_list(Request $req)
    {
        $attributeNames = array(
           'vision' => 'Vision',
           'mission' => 'Mission',
        );
            $rules = array(
                'vision'             => 'nullable|min:10',
                'mission'             => 'nullable|min:10',
            );


            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/vmission_list')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = Vision_Mission::where('ssk_uid',$id)->first();
                $vid->vision = Input::get('vision');
                $vid->mission = Input::get('mission');
                $vid->save();
            }
            return redirect()->back()->with('status', 'Your vision and mission has been saved Suceessfully');
    }

    public function delvm_list()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = Vision_Mission::where('ssk_uid',$id)->first();
                if(isset($vid))
                {
                    return view('delete_vision_mission')->with('vm',$vid);
                }
        return view('delete_vision_mission')->with('vm',new Vision_Mission);
    }

    public function delete_vmission_list(Request $req)
    {
        
            $rules = array(
                'skid'             => 'required',
            );


            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/delvm_list')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = Vision_Mission::where('ssk_id',Input::get('skid'))->first();
                $vid->delete();
            }
            return redirect()->back()->with('status', 'Your vision and mission has been deleted Suceessfully');
    }


    public function add_ourworks(){
        return view('sskindia.add_works');
    }


    public function save_works(Request $req){

        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                
                for ($i=0; $i < sizeof(Input::get('servicename')); $i++) { 
                    # code...
                    $workdata = new SskWork;
                    $workdata->work_title = (String)Input::get('servicename')[$i];
                    $workdata->work_desc = (String)Input::get('servicedesc')[$i];
                    $workdata->u_id = $id;
                    $workdata->save();

                }
            
                
                return redirect()->back()->with('status', 'Work details added successfully' );

    }


    public function edit_ourworks()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $workdata = SskWork::where('u_id',$id)->get();
        return view('sskindia.edit_ourworks')->with('workdata',$workdata);
    }

    public function save_editworks()
    {
        
              $rules = array(
                'sswork'             => 'required',
            );


            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/edit_ourworks')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = SskWork::where('id',Input::get('sswork'))->first();
                $vid->work_title=Input::get('servicename');
                $vid->work_desc=Input::get('servicedesc');
                $vid->save();
            }
            return redirect()->back()->with('status', 'Your Work details has been saved Suceessfully'); 
                
                
    }

    public function del_ourworks()
    {
        $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $workdata = SskWork::where('u_id',$id)->get();
        return view('sskindia.del_ourworks')->with('workdata',$workdata);
    }

    public function delete_works()
    {
        
              $rules = array(
                'sswork'             => 'required',
            );


            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/del_ourworks')
                    ->withErrors($validator);

            } else {
                $data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $vid = SskWork::where('id',Input::get('sswork'))->first();
                $vid->delete();
            }
            return redirect()->back()->with('status', 'Your Work details has been deleted Suceessfully');     
    }


    public function edit_photo(){
        echo "HIIII";
    }


}
