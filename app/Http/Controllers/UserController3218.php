<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;
use App\User;
use App\AboutModel;
use App\ServiceModel;
use Validator;
use App\ContactModel;
use App\SubAdminModel;
use App\LatestNewsModel;
use App\DTHModel;

class UserController extends Controller
{
    //
	public function __construct() {
        $this->middleware('auth');
    }


    public function logo_add() {

    	$data = session()->all();

        $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $logo = User::where('id',$id)->first();
        
		return view('admin_rw.logo_manipulations')->with('logo',$logo);

    }

    public function save_logo_add(Request $request) {
		$data = $request->image;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$data = base64_decode($data);
		$image_name = time() . 'logo-' . $id . '.png';
		$path = public_path() . "/uploads//logo//" . $image_name;

		file_put_contents($path, $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$userdata = User::where('id',$id)->update(['logo'=>$image_name]);

		return response()->json(['success'=>'done']);
    }


    public function about_add() {

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

    	$about_data = AboutModel::where('user_id', $id)->first();

    	return view('admin_rw.about_us')->with('about_data', $about_data);
    }

    public function save_about_add() {
		$attributeNames = array(
		    'aboutusdesc'             => 'About Description'
		);
		$rules = array(
		    'aboutusdesc'             => 'required|min:20'
		);

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributeNames);
		if ($validator->fails()) {

		    $messages = $validator->messages();
		    return Redirect::to('/admin/about_add')
		        ->withErrors($validator);

		} else {
		    $data = session()->all();
		    $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

	    	$about_data = AboutModel::where('user_id', $id)->first();

	    	$aboutusdesc = Input::get('aboutusdesc');

		    if(isset($about_data->about_id)){
		    	 $about1 = AboutModel::where('user_id', $id)->update(['about_desc' => $aboutusdesc]);
		    }
		    else{
		    	 $about1 = new AboutModel;
				 $about1->about_desc = Input::get('aboutusdesc');
				 $about1->user_id = $id;
				 $about1->save();	
		    }
		   
		    
		}

		return redirect()->back()->with('status', 'About Description data has been saved successfully');
    }


    public function service_add() {
    	return view('admin_rw.services');
    }

    public function save_service_add(Request $req) {
		$data = $req->image;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$data = base64_decode($data);
		$image_name = time(). 'service_img-' . $id .'.png';
		$path = public_path() . "/uploads//services//" . $image_name;

		file_put_contents($path, $data);

		$service_data = new ServiceModel;
		$service_data->service_name = Input::get('tnm');
		$service_data->service_desc = Input::get('tps');   
		$service_data->service_desc_detail = Input::get("service_brief");            
		$service_data->service_img = $image_name;
		$service_data->u_id = $id;
		$service_data->save();
    }

    public function service_update(){

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.services_update')->with('service_data', $service_data);

    }

    public function save_service_update(){

    	 $rules = array(
                'teammemnm'             => 'nullable|min:2',
                'teammempos'             => 'nullable|min:2',
                'service_desc_detail'             => 'nullable|min:2'  
            );


            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_list')
                    ->withErrors($validator);

            } else {
                    
               	$data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $service_data = ServiceModel::where('id',Input::get('id'))->first();
                $service_data->service_name     = Input::get('teammemnm');
                $service_data->service_desc     = Input::get('teammempos');
                $service_data->service_desc_detail     = Input::get('service_desc_detail');
                $service_data->save();

                
                return redirect()->back()->with('status', 'Service details updated successfully' );
        }

     }   


     public function edit_service_photo(){
     	$service_data = ServiceModel::where('id', Input::get("id"))->first();
     	return view('admin_rw.edit_service_photo')->with('id', Input::get("id"))->with('service_data', $service_data);
     }

     public function edit_service_photo_save(Request $request){
     	 $data = $request->image;
        $id = Input::get('id');

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);
        $image_name = time(). '-' . $id .'.png';
        $path = public_path() . "/uploads//services//" . $image_name;

        file_put_contents($path, $data);

        $userdata = ServiceModel::where('id',$id)->update(['service_img'=>$image_name]);

        return response()->json(['success'=>'done']);
     }

     public function service_delete(){

		$data = session()->all();
		$id = $data["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];
		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.delete_service')->with('service_data', $service_data);

     }

     public function save_service_delete(Request $request){
     	ServiceModel::where('id', Input::get("id"))->delete();
     	return redirect()->back()->with('status', 'Service deleted successfully');
     }


     public function view_contacts(){
     	$data_contactus = ContactModel::all();
     	return view('admin_rw.view_contacts')->with('data_contactus', $data_contactus);
     }

     public function new_old_user() {
        return view('admin_rw.login_register');
    }

    public function save_website_user(Request $request) {

        $id_type = 'mobile_number';
        $user_phone = Input::get("mobile_no");
        // $init_id = '9910028267';
        $name = Input::get("username");
        $email = Input::get("email");
   		$password = Input::get("password");
   		$user_type = Input::get("type_customer");
        $panno = Input::get("panno");
        $address = Input::get("address");

   		$subadmindata = SubAdminModel::where('email', $email)->orWhere('user_phone', $user_phone)->first();

   		if(!isset($subadmindata->id)) {
			if($user_type == 'NA') {
				return redirect()->back()->with('error', 'Please select a valid user type');
			}
			else {
				$website_user_data = new SubAdminModel;
				$website_user_data->name = $name;
				$website_user_data->email = $email;
				$website_user_data->user_phone = $user_phone;
				$website_user_data->password = bcrypt($password);
				$website_user_data->user_type = $user_type;
                $website_user_data->address = $address;
                $website_user_data->panno = $panno;
                $website_user_data->is_verified = 1;
				$website_user_data->save();
				return redirect()->back()->with('status', 'User added successfully!');
			}
   		}
   		else {
			return redirect()->back()->with('error', 'User Email/Mobile No already exists!');
   		}

    }

    public function enter_otp(Request $request) {

                 $data = $request->session()->all();
    
                  echo $data["otp"];

         return view('admin_rw.enter_otp');
    }

     public function verify_user(Request $request) {


           $data = $request->session()->all();

        $name = $data["name"];
        $email = $data["email"];
        $user_phone = $data["user_phone"];
        $password = $data["password"];

        // $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/verification/otp:";  //Preprod URL

        $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/verification/otp:";

        $otp =Input::get("otp");
        $id = $user_phone;
            // $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
            // $init_id = '9910028267';

        $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials

        $init_id = '9923081299';
        $id_type = 'mobile_number';

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

        $url = $url . $otp;

         $bodyParam = "id=" . $id . "&id_type=" . $id_type . "&otp=" . $otp . "&initiator_id=" . $init_id;
            
    
        try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
	            CURLOPT_PORT => "25002",
	            CURLOPT_URL => $url,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_ENCODING => "",
	            CURLOPT_MAXREDIRS => 10,
	            CURLOPT_TIMEOUT => 30,
	            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	            CURLOPT_CUSTOMREQUEST => "PUT",
	            CURLOPT_POSTFIELDS => $bodyParam,
	            CURLOPT_SSL_VERIFYPEER => false,
	            CURLOPT_SSL_VERIFYHOST => false,
	            CURLOPT_HTTPHEADER => array(
	            "cache-control: no-cache",
	            "content-type: application/x-www-form-urlencoded",
	            "developer_key:" . $dev_key,
	            "secret-key:" . $secret_key,
	            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        // var_dump($json_retrieve);

        if($json_retrieve->status == 0) {

            $website_user_data = new SubAdminModel;
            $website_user_data->name = $name;
            $website_user_data->email = $email;
            $website_user_data->user_phone = $user_phone;
            $website_user_data->password = bcrypt($password);
            $website_user_data->save();

            return redirect()->back()->with('status', 'Verified successfully!');
        }
        else {
            return redirect()->back()->with('error', 'Error occured while verifying!');
        }


        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }


    }


    public function verify_users(Request $req) {
        $unverified_users = SubAdminModel::where('is_verified', '!=', '1')->orWhere('is_verified', NULL)->get();
        return view('admin_rw.unverified_users')->with('unverified_users', $unverified_users);
    }

    public function view_users(Request $req) {
        $view_users = SubAdminModel::all();
        return view('admin_rw.view_users')->with('view_users', $view_users);
    }

    public function verify_apguser(Request $req) {
        $userid = Input::get("userid");

        SubAdminModel::where('id', $userid)->update(["is_verified" => 1]);

        return redirect()->back()->with('status', "Status updated successfully!");
    }


    public function add_updates(Request $request) {
      return view('admin_rw.add_updates');
    }

     public function save_updates(Request $request) {

        $newsdata = new LatestNewsModel;
        $newsdata->news_desc = Input::get('newsdesc');
        $newsdata->save();

        return redirect()->back()->with('status', 'News added successfully!');

    } 

     public function update_news(Request $request) {

        $news_data = LatestNewsModel::all();
        return view('admin_rw.news_list')->with('news_data', $news_data);
    }

     public function save_update_news(Request $request) {

        LatestNewsModel::where('id', Input::get('newsid'))->update(['news_desc' => Input::get("newsdesc")]);

        return redirect()->back()->with('status', 'News updated successfully!');

    }

     public function delete_news(Request $request) {
        $news_data = LatestNewsModel::all();
        return view('admin_rw.delete_news')->with('news_data', $news_data);
    }

     public function save_delete_news(Request $request) {
              LatestNewsModel::where('id', Input::get("newsid"))->delete();
              return redirect()->back()->with('status', 'News deleted successfully!');
    } 


    public function set_dth_commission(Request $request) {
      return view('admin_rw.setdthcommission');
    }

    public function save_set_dth_commission(Request $request) {

       $dthdata = new DTHModel;
       $dthdata->operator_name = Input::get("opname");
       $dthdata->rate = Input::get("rate");
       $dthdata->commission = Input::get("commission");
       $dthdata->amount = Input::get("amount");
       $dthdata->save();

       return redirect()->back()->with('status', 'Operator details saved successfully!');

    }


    public function set_update_dth_commission() {
      $dthdata = DTHModel::all();
      return view('admin_rw.updatedthcommission')->with('dthdata', $dthdata);
    }

    public function save_update_dth_commission(Request $request) {
        DTHModel::where('id', Input::get("id"))->update(['operator_name' => Input::get("operator_name"), 'rate' => Input::get("rate"), 'commission' => Input::get("commission"), 'amount' => Input::get("amount")]);

        return redirect()->back()->with('status', 'Details updated successfully!');
    }


    public function savegoto(Request $request) {
      $routeto = 'admin/goto_dthupdate/' . Input::get("id");
      return redirect($routeto);
    }


    public function goto_dthupdate($q) {
                $dthdata = DTHModel::where('id', $q)->first();
              return view('admin_rw.updategotodth')->with('dthdata', $dthdata);
    }

    
}
