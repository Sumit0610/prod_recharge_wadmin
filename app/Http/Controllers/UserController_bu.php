<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;
use App\User;
use App\AboutModel;
use App\ServiceModel;
use Validator;
use App\ContactModel;


class UserController extends Controller
{
    //
	public function __construct() {
        $this->middleware('auth');
    }


    public function logo_add() {

    	$data = session()->all();

        $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
        $logo = User::where('id',$id)->first();
        
		return view('admin_rw.logo_manipulations')->with('logo',$logo);

    }

    public function save_logo_add(Request $request) {
		$data = $request->image;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$data = base64_decode($data);
		$image_name = time() . 'logo-' . $id . '.png';
		$path = public_path() . "/uploads//logo//" . $image_name;

		file_put_contents($path, $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$userdata = User::where('id',$id)->update(['logo'=>$image_name]);

		return response()->json(['success'=>'done']);
    }


    public function about_add() {

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

    	$about_data = AboutModel::where('user_id', $id)->first();

    	return view('admin_rw.about_us')->with('about_data', $about_data);
    }

    public function save_about_add() {
		$attributeNames = array(
		    'aboutusdesc'             => 'About Description'
		);
		$rules = array(
		    'aboutusdesc'             => 'required|min:20'
		);

		$validator = Validator::make(Input::all(), $rules);
		$validator->setAttributeNames($attributeNames);
		if ($validator->fails()) {

		    $messages = $validator->messages();
		    return Redirect::to('/admin/about_add')
		        ->withErrors($validator);

		} else {
		    $data = session()->all();
		    $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

	    	$about_data = AboutModel::where('user_id', $id)->first();

	    	$aboutusdesc = Input::get('aboutusdesc');

		    if(isset($about_data->about_id)){
		    	 $about1 = AboutModel::where('user_id', $id)->update(['about_desc' => $aboutusdesc]);
		    }
		    else{
		    	 $about1 = new AboutModel;
				 $about1->about_desc = Input::get('aboutusdesc');
				 $about1->user_id = $id;
				 $about1->save();	
		    }
		   
		    
		}

		return redirect()->back()->with('status', 'About Description data has been saved successfully');
    }


    public function service_add() {
    	return view('admin_rw.services');
    }

    public function save_service_add(Request $req) {
		$data = $req->image;

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$data = base64_decode($data);
		$image_name = time(). 'service_img-' . $id .'.png';
		$path = public_path() . "/uploads//services//" . $image_name;

		file_put_contents($path, $data);

		$service_data = new ServiceModel;
		$service_data->service_name = Input::get('tnm');
		$service_data->service_desc = Input::get('tps');   
		$service_data->service_desc_detail = Input::get("service_brief");            
		$service_data->service_img = $image_name;
		$service_data->u_id = $id;
		$service_data->save();
    }

    public function service_update(){

		$datas = session()->all();
		$id =  $datas['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];

		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.services_update')->with('service_data', $service_data);

    }

    public function save_service_update(){

    	 $rules = array(
                'teammemnm'             => 'nullable|min:2',
                'teammempos'             => 'nullable|min:2',
                'service_desc_detail'             => 'nullable|min:2'  
            );


            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                $messages = $validator->messages();
                return Redirect::to('/admin/about_list')
                    ->withErrors($validator);

            } else {
                    
               	$data = session()->all();
                $id =  $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
                $service_data = ServiceModel::where('id',Input::get('id'))->first();
                $service_data->service_name     = Input::get('teammemnm');
                $service_data->service_desc     = Input::get('teammempos');
                $service_data->service_desc_detail     = Input::get('service_desc_detail');
                $service_data->save();

                
                return redirect()->back()->with('status', 'Service details updated successfully' );
        }

     }   


     public function edit_service_photo(){
     	$service_data = ServiceModel::where('id', Input::get("id"))->first();
     	return view('admin_rw.edit_service_photo')->with('id', Input::get("id"))->with('service_data', $service_data);
     }

     public function edit_service_photo_save(Request $request){
     	 $data = $request->image;
        $id = Input::get('id');

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $data = base64_decode($data);
        $image_name = time(). '-' . $id .'.png';
        $path = public_path() . "/uploads//services//" . $image_name;

        file_put_contents($path, $data);

        $userdata = ServiceModel::where('id',$id)->update(['service_img'=>$image_name]);

        return response()->json(['success'=>'done']);
     }

     public function service_delete(){

		$data = session()->all();
		$id = $data["login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d"];
		$service_data = ServiceModel::where('u_id', $id)->get();

		return view('admin_rw.delete_service')->with('service_data', $service_data);

     }

     public function save_service_delete(Request $request){
     	ServiceModel::where('id', Input::get("id"))->delete();
     	return redirect()->back()->with('status', 'Service deleted successfully');
     }


     public function view_contacts(){
     	$data_contactus = ContactModel::all();
     	return view('admin_rw.view_contacts')->with('data_contactus', $data_contactus);
     }

     public function new_old_user() {
        return view('admin_rw.login_register');
    }

    public function save_website_user(Request $request) {

        $id_type = 'mobile_number';
        $id = Input::get("mobile_no");
        // $init_id = '9910028267';
        $name = Input::get("username");
        // $secret_key = 'Z86hswK9xE4Ap8kQV7i++JtEQV1tKSOMfjfYYCpG0='; 
        // $secret_key_timestamp = round(microtime(true) * 1000); 

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

        // $dev_key = 'becbbce45f79c6f5109f848acd540567';

        // $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
        // $init_id = '9910028267';

        $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials
        $init_id = '9923081299';

        // $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/";       //Preprod URL
           $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/";
        $url = $url . $id_type . ":" . $id;

        $bodyParam = "customer_id_type=" . $id_type . "&id=" . $id . "&initiator_id=" . $init_id . "&name=" . $name; 

        try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25002",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key,
            "secret-key:" . $secret_key,
            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        var_dump($response);
        var_dump($json_retrieve);


        if($json_retrieve->status == 0) {

            $request->session()->put('name', Input::get("username"));
            $request->session()->put('email', Input::get("email"));
            $request->session()->put('user_phone', Input::get("mobile_no"));
            $request->session()->put('password', Input::get("password"));
            //$request->session()->put('otp', $json_retrieve->data->otp);     // not sent in prod
            $request->session()->put('otp', "prod");

            return redirect('enter_otp');

        }
        else if($json_retrieve->status == 17) {
             return redirect()->back()->with('error', 'Mobile number already registered!');
        }
        else {
             return redirect()->back()->with('error', 'Error occured while registering!');
        }
        

        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }

        // $website_user_data = new SubAdminModel;
        // $website_user_data->name = Input::get("username");
        // $website_user_data->email = Input::get("email");
        // $website_user_data->user_phone = Input::get("mobile_no");
        // $website_user_data->password = bcrypt(Input::get("password"));
        // $website_user_data->save();

        // return redirect()->back()->with('status', 'User registered succesfully!');
    }

    public function enter_otp(Request $request) {

                 $data = $request->session()->all();
    
                  echo $data["otp"];

         return view('admin_rw.enter_otp');
    }

     public function verify_user(Request $request) {


           $data = $request->session()->all();

        $name = $data["name"];
        $email = $data["email"];
        $user_phone = $data["user_phone"];
        $password = $data["password"];

        // $url = "https://staging.eko.co.in:25004/ekoapi/v1/customers/verification/otp:";  //Preprod URL

        $url = "https://api.eko.co.in:25002/ekoicici/v1/customers/verification/otp:";

        $otp =Input::get("otp");
        $id = $user_phone;
            // $dev_key = 'becbbce45f79c6f5109f848acd540567';    //Preprod credentials
            // $init_id = '9910028267';

        $dev_key = '90beb0087a11fd0361b47d1d5ab77ec5';    //Prod credentials

        $init_id = '9923081299';
        $id_type = 'mobile_number';

        $key = 'e9de289f-8865-4305-9765-aad144e98f74';
        $encodedKey = base64_encode($key);

        $secret_key_timestamp = round(microtime(true) * 1000); 

        $signature = hash_hmac('SHA256', $secret_key_timestamp, $encodedKey, true);

        $secret_key = base64_encode($signature);

        $url = $url . $otp;

         $bodyParam = "id=" . $id . "&id_type=" . $id_type . "&otp=" . $otp . "&initiator_id=" . $init_id;
            
    
        try {
        $curl = curl_init();

        if (FALSE === $curl)
        throw new Exception('failed to initialize');

            curl_setopt_array($curl, array(
            CURLOPT_PORT => "25002",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $bodyParam,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "developer_key:" . $dev_key,
            "secret-key:" . $secret_key,
            "secret-key-timestamp:" . $secret_key_timestamp
            ),
            ));

        $response = curl_exec($curl);

        $json_retrieve = json_decode($response, false);

        // var_dump($json_retrieve);

        if($json_retrieve->status == 0) {

            $website_user_data = new SubAdminModel;
            $website_user_data->name = $name;
            $website_user_data->email = $email;
            $website_user_data->user_phone = $user_phone;
            $website_user_data->password = bcrypt($password);
            $website_user_data->save();

            return redirect()->back()->with('status', 'Verified successfully!');
        }
        else {
            return redirect()->back()->with('error', 'Error occured while verifying!');
        }


        if (FALSE === $response)

            throw new \Exception(curl_error($curl), curl_errno($curl));

        } catch(\Exception $e) {

            trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);

        }


    }

    
}
