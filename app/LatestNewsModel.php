<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestNewsModel extends Model
{
    //
    protected $table = "latest_news";
}
