<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDepositModel extends Model
{
    //
    protected $connection = "mysql2";
    protected $table = "request_deposit";
}
