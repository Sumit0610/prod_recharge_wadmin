<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellDTHModel extends Model
{
    //
    protected $table = "sell_dth";
    protected $connection = "mysql2";
}
