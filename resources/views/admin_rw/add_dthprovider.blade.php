@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')
    <h1><b>Add DTH provider</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@section('content')

  <div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('/save_website_user') }}" method="post" role="form">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Full Name" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="Mobile Number" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="Complete Address" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="panno" id="panno" tabindex="1" class="form-control" placeholder="PAN Card No" value="">
                                    </div>
                                     <div class="form-group">
                                     <label for="type_customer" class="col-md-12">Customer type</label>
                                    <select name="type_customer" id="type_customer">
                                        <option value="NA">-------Select an option-------</option>
                                        <option value="0">Master Distributor</option>
                                        <option value="1">Distributor</option>
                                        <option value="2">Retailer</option>

                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                    </div>
                                    
                                    <div class="form-group">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Add User" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>
    
    

      

@stop