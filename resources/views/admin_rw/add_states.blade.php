@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')
    <h1><b>Add States</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@section('content')

  <div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('/admin/save_add_states') }}" method="post" role="form">
                                    <div class="form-group">
                                        <input type="text" name="statename" id="statename" tabindex="1" class="form-control" placeholder="State Name" value="">
                                    </div>
                                    
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    
                                    <div class="form-group">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Add States" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div>
    
    

      

@stop