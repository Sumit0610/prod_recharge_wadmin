@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Add to wallet</b></h1>
@stop

@section('content')


<html lang="en">
<head>
  <title></title>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  </head>


<body>
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading"></div>
      <div class="panel-body">


            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif


        <form action="{{ url('/admin/save_add_to_wallet') }}" enctype="multipart/form-data" method="post">

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

             <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="username">MD, Distributors & Retailers</label>
                  <select name="username" id="username" class="col-md-12">
                      <option value="NA" selected>-------Select user-------</option>
                      @foreach($subadmins as $key => $value) 
                          <option value="{{ $value->id }}">
                          {{ $value->name }} - {{ $value->user_phone }}. Wallet: {{ $value->wallet_balance }}
                          </option>
                      @endforeach
                  </select>
                </div>

                 <div class="form-group col-md-8">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Amount</dt></label>
                  
                  <input type="text" class="form-control" name="amount" id="amount">

                </div>
                
             </div>

             <br>
             <div class="col-md-12">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Add to wallet</button>
                </div>
             </div>

          </form>

      </div>
    </div>
</div>


</body>
</html>

@stop