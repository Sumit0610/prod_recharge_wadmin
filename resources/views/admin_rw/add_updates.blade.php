@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Add About Us details</b></h1>
@stop

@section('content')


<html lang="en">
<head>
  <title></title>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.5.1/croppie.css">
</head>


<body>
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading"></div>
      <div class="panel-body">


             @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

        
        <form action="{{ url('/admin/save_updates') }}" enctype="multipart/form-data" method="post">

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        
             <div class="col-md-12">
                <div class="form-group">
                  <label for="newsdesc" class="col-lg-5 control-label"><dt>Latest News</dt></label>
<textarea class="form-control" id="newsdesc" placeholder="Enter News description" name="newsdesc" rows="4"></textarea>
                </div>
                
             </div>

             <br>
             <div class="col-md-12">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Save this as Latest News</button>
                </div>
             </div>

          </form>


        


      </div>
    </div>
</div>



</body>
</html>

@stop