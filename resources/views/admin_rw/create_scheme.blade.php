@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Create Scheme</b></h1>
@stop

@section('content')


<html lang="en">
<head>
  <title></title>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  </head>


<body>
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading"></div>
      <div class="panel-body">


            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

        
        <form action="{{ url('/admin/create_scheme') }}" enctype="multipart/form-data" method="post">

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" name="commisionid" value="">
        
             
             <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="schemename">Scheme Name</label>
                   <input type="text" name="schemename" id="schemename" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="rechargecommission">Recharge Commission(In %)</label>
                  <input type="text" name="rechargecommission" id="rechargecommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <!-- <div class="col-md-12">
                <div class="form-group col-md-8">
                   <label for="dthcommission">DTH Commission(In %)</label>
                  <input type="text" name="dthcommission" id="dthcommission" class="form-control col-md-12" value="" required>
                </div>
             </div> -->

             <div class="col-md-12">
                <div class="form-group col-md-8">
                  <label for="moneytransfercommission">Money Transfer Commission(In %)</label>
                  <input type="text" name="moneytransfercommission" id="moneytransfercommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <div class="col-md-12">
                <div class="form-group col-md-8">
                  <label for="pancardcommission">Pan card Commission(In %)</label>
                  <input type="text" name="pancardcommission" id="pancardcommission" class="form-control col-md-12" value="" required>
                </div>
             </div>

             <br>
             <div class="col-md-12">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Create Scheme</button>
                </div>  
             </div>
  
          </form>

      </div>
    </div>
</div>


</body>
</html>

@stop