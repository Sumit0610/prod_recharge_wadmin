@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Delete News</b></h1>
@stop

@section('content')



	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->
          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif


			<div class="box-body">
 			

             @foreach($news_data as $key => $tem)
             @if($tem->news_desc!=null || $tem->news_desc!=NULL || $tem->news_desc!='')
             <div class="col-md-8">
                <h4>News posted</h4>
                <p>{{ $tem->news_desc }}</p>
                <form onsubmit="return confirm('Do you really want to delete this news?');" action="{{ url('/admin/save_delete_news') }}" enctype="multipart/form-data" method="post" name="delabout_team" id="delabout_team">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" name="newsid" value="{{$tem->id}}">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Delete this news</button>
                  
                </div>
                </form>
             </div>
             @endif
             @endforeach
           
            
             <br>
         
            </div>

		   


     </div>
    </div>
    
    

      

@stop