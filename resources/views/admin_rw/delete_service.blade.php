@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Delete Service details</b></h1>
@stop

@section('content')



	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->
          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif


			<div class="box-body">
 				


             @foreach($service_data as $key => $tem)
             @if($tem->service_name!=null || $tem->service_name!=NULL || $tem->service_name!='')
             <div class="col-md-8">
                <h4>Service Details</h4>
                <p>{{ $tem->service_name }}</p>
                <form onsubmit="return confirm('Do you really want to delete this service?');" action="{{ url('/admin/service_delete') }}" enctype="multipart/form-data" method="post" name="delabout_team" id="delabout_team">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" name="id" value="{{$tem->id}}">
                <div class="form-group">
                  <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>  Delete this service</button>
                  
                </div>
                </form>
             </div>
             @endif
             @endforeach
           
            
             <br>
         
            </div>

		   


     </div>
    </div>
    
    

      

@stop