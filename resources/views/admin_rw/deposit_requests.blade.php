@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Deposit Requests</b></h1>
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@section('content')

	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp;About Details List</h2>
 -->


          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
			@if(session('error'))
				<div class="alert alert-danger">
				{{ session('error') }}
				</div>
			@endif

			<div class="box-body"> 				

             @if(!$deposit_requests->isEmpty())
             @foreach($deposit_requests as $key => $tem)
             <form action="{{ url('/admin/save_goto_deposit_requests') }}" enctype="multipart/form-data" method="post">
             <input type="hidden" name="_token" value="{!! csrf_token() !!}">
             <input type="hidden" name="id" value="{{ $tem->id }}">

             <div class="col-md-8">
	            <div class="form-group">
	              <label for="operator_name" class="col-lg-5 control-label"><dt>User Details</dt></label>
	              <input type="text" class="form-control" id="operator_name" placeholder="" name="operator_name" value="{{$tem->name}} - {{ $tem->user_phone }}" required readonly>
	            </div>
             </div>
            
             <div class="col-md-8">
                <div class="form-group">
                  <label for="operator_name" class="col-lg-5 control-label"><dt>Requested on</dt></label>
                  <input type="text" class="form-control" id="operator_names" placeholder="Enter Operator Name" name="operator_names" value="{{$tem->created_at}}" required readonly>
                </div>
             </div>

             <div class="col-md-8">
              <div class="form-group">
                <label for="operator_name" class="col-lg-5 control-label"><dt>Wallet balance</dt></label>
                <input type="text" class="form-control" id="wallet_balance" placeholder="" name="wallet_balance" value="{{$tem->wallet_balance}}" required readonly>
              </div>
             </div>

             <div class="col-md-8">
              <div class="form-group">
                <label for="operator_name" class="col-lg-5 control-label"><dt>Deposit Amount</dt></label>
                <input type="text" class="form-control" id="wallet_balance" placeholder="" name="wallet_balance" value="{{$tem->amount}}" required readonly>
              </div>
             </div>

             <div class="col-md-8">
                <div class="form-group">
                  <label for="operator_name" class="col-lg-5 control-label"><dt>Status</dt></label>

                  @if($tem->status == '1')
                      <input type="text" class="form-control" id="operator_names" placeholder="Enter Operator Name" name="operator_names" value="Deposited" required readonly>
                  @elseif($tem->status == '2')
                     <input type="text" class="form-control" id="operator_names" placeholder="Enter Operator Name" name="operator_names" value="Rejected" required readonly> 
                  @else
                      <input type="text" class="form-control" id="operator_names" placeholder="Enter Operator Name" name="operator_names" value="No Action" required readonly>                   
                  @endif    
                </div>

             </div>
            
             <br>
             <div class="col-md-8">
	            <div class="form-group">
	              <button type="submit" class="btn btn-info" name="sub1">
                  @if(($tem->status == '1') || ($tem->status == '2'))
                    Click to view details  
                  @else
                    Update status
                  @endif
                  </button>
	            </div><br>
             </div>           

            </form>
             
             @endforeach

             @endif


              

     </div>
    </div>

@stop