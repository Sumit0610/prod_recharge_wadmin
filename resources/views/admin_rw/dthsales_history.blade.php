@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>DTH Sales History</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif
        

      @foreach ($dthdata as $key => $value) 

            
      <!-- <form action="{{ url('/admin/service_list') }}" enctype="multipart/form-data" method="post">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="sid" value="{{ $value->id }}"> -->

      <a href="{{ url('/admin/dthsaleshist/'.$value->id) }}">

      <div class="card">
        <div class="container">

              <div class="col-md-9">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Date (Details saved) </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Data entered date" name="servicename" value="{{$value->created_at}}">
                </div>

               </div>

           @if(isset($value->name))
          <div class="col-md-9">
            <div class="form-group">
              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Agent Name </dt></label>
          
              <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="{{$value->name}} - {{$value->user_phone}}">
            </div>

           </div>
        @endif
  
           @if(isset($value->operator_name))
              <div class="col-md-9">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Operator Name </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="{{$value->operator_name}}">
                </div>

               </div>
            @endif

            <!-- @if(isset($value->rate))
               <div class="col-md-4">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Rate </dt></label>
              
                  <input type="text" readonly class="form-control" id="rate" placeholder="Rate" name="rate" value="{{$value->rate}}">
                </div>

               </div>
             @endif

             @if(isset($value->commission))
               <div class="col-md-2">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Commission </dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="Commission" name="subject_message" value="{{$value->commission}}">
                </div>

               </div>


             @endif 

             @if(isset($value->amount))
               <div class="col-md-3">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Amount</dt></label>
                  
                   <input type="text" readonly class="form-control" id="servicedesc" placeholder="Amount" name="servicedesc" value="{{$value->amount}}">

                </div>
               </div>

             @endif 

             @if(isset($value->buyername))
               <div class="col-md-9">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt>Buyer Name</dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="Buyer Name" name="subject_message" value="{{$value->buyername}}">
                </div>

               </div>


             @endif 

             @if(isset($value->installation_address))
               <div class="col-md-9">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Installation Address </dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="Installation Address" name="subject_message" value="{{$value->installation_address}}">
                </div>

               </div>


             @endif 

             @if(isset($value->city) && isset($value->pincode))
               <div class="col-md-9">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> City </dt></label>
              
                  <input type="text" readonly class="form-control" id="city" placeholder="City" name="city" value="{{$value->city}}-{{ $value->pincode }}">
                </div>

               </div>

             @endif 

             @if(isset($value->mobile_no))
               <div class="col-md-9">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> Contact Details </dt></label>
                  
                  @if($value->altmobile_no != NULL)
                     <input type="text" readonly class="form-control" id="subject_message" placeholder="Contact Details" name="subject_message" value="{{$value->mobile_no}} / {{$value->altmobile_no}} ">
                  @else 
                    <input type="text" readonly class="form-control" id="subject_message" placeholder="Contact Details" name="subject_message" value="{{$value->mobile_no}}">
                  @endif    
                </div>

               </div>


             @endif  -->

             </div></div>      

             </a>

         @endforeach     

            

      </div>

      

    

     </div>
    </div>

@stop