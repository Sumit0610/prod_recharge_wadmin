@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')

    <h1><b>DTH Sales History</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css"><style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style></style>


@section('content')

	<div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    
                                     <div class="form-group col-md-8">
                                     <label for="operator">DTH Operator</label>
                                             {{ $dthdata->operator_name }}
                                    </div>
                                   

                                    <div class="form-group col-md-8">
                                                                         <label for="operator">Rate</label>

                                        <input type="text" name="rate" id="rate" tabindex="1" class="form-control" placeholder="Rate" value="{{ $dthdata->rate }}" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                         <label for="operator">Commission</label>
                                    <input type="text" name="commission" id="commission" tabindex="1" class="form-control" placeholder="Commission" readonly value="{{ $dthdata->commission }}">
                                    </div>
                                    <div class="form-group col-md-8">
                                      <label for="operator">Amount</label>
                                       <input type="text" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount" value="{{ $dthdata->amount }}" readonly>
                                    </div>
                                    <div class="form-group col-md-8">
                                      <label for="operator">Buyer Name</label>
                                       <input type="text" name="buyername" id="buyername" tabindex="1" class="form-control" placeholder="Buyer Name" readonly value="{{ $dthdata->buyername }}">
                                    </div>
                                    <div class="form-group col-md-8">
                                      <label for="operator">Installation Address</label>
                                       <input type="text" name="installation_address" id="installation_address" tabindex="1" class="form-control" placeholder="Installation Address" value="{{ $dthdata->installation_address }}" readonly>
                                    </div>
                                
                                    <div class="form-group col-md-8">
                                      <label for="operator">City</label>
                                       <input type="text" name="city" id="city" tabindex="1" class="form-control" placeholder="City" value="{{ $dthdata->city }}" readonly>
                                    </div>

                                    <div class="form-group col-md-8">
                                      <label for="operator">Postal Code</label>
                                       <input type="number" name="pincode" id="pincode" tabindex="1" class="form-control" placeholder="Pincode" value="{{ $dthdata->pincode }}" readonly>
                                    </div>

                                    <div class="form-group col-md-8">
                                      <label for="operator">Contact No</label>
                                       <input type="number" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="Customer Mobile Number" value="{{ $dthdata->mobile_no }}" readonly>
                                    </div>

                                    <div class="form-group col-md-8">
                                        <input type="number" name="altmobile_no" id="altmobile_no" tabindex="1" class="form-control" placeholder="Customer Mobile Number(Alternate if any)" value="{{ $dthdata->altmobile_no }}" readonly>
                                    </div>
                                    
                            
                                
                            </div>
                        </div>
          
        </div></div>


    

      

@stop