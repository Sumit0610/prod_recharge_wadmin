<!DOCTYPE html>
<html class="no-js" style="height: 100%;">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Recharge Website</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        

        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="{{asset('website_template/plugins/bootstrap/bootstrap.min.css')}}">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{asset('website_template/plugins/ionicons/ionicons.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{asset('website_template/plugins/animate-css/animate.css')}}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{asset('website_template/plugins/slider/slider.css')}}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{asset('website_template/plugins/owl-carousel/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('website_template/plugins/owl-carousel/owl.theme.css')}}">
        <!-- Fancybox -->
        <link rel="stylesheet" href="{{asset('website_template/plugins/facncybox/jquery.fancybox.css')}}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{asset('website_template/css/style.css')}}">

<style type="text/css">
            
            body {
    padding-top: 90px;
}
.panel-login {
    border-color: #ccc;
    -webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
    box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
    color: #00415d;
    background-color: #fff;
    border-color: #fff;
    text-align:center;
}
.panel-login>.panel-heading a{
    text-decoration: none;
    color: #666;
    font-weight: bold;
    font-size: 15px;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
    color: #029f5b;
    font-size: 18px;
}
.panel-login>.panel-heading hr{
    margin-top: 10px;
    margin-bottom: 0px;
    clear: both;
    border: 0;
    height: 1px;
    background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
    background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
    height: 45px;
    border: 1px solid #ddd;
    font-size: 16px;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
    outline:none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    border-color: #ccc;
}
.btn-login {
    background-color: #59B2E0;
    outline: none;
    color: #fff;
    font-size: 14px;
    height: auto;
    font-weight: normal;
    padding: 14px 0;
    text-transform: uppercase;
    border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
    color: #fff;
    background-color: #53A3CD;
    border-color: #53A3CD;
}
.forgot-password {
    text-decoration: underline;
    color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
    text-decoration: underline;
    color: #666;
}

.btn-register {
    background-color: #1CB94E;
    outline: none;
    color: #fff;
    font-size: 14px;
    height: auto;
    font-weight: normal;
    padding: 14px 0;
    text-transform: uppercase;
    border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
    color: #fff;
    background-color: #1CA347;
    border-color: #1CA347;
}


</style>

    </head>
    <body style="height: 50%;">

<div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="#" class="active" id="login-form-link" class="text-center">Register</a>
                            </div>
                            
                        </div>
                        <hr>
                    </div>
                    @if(session('status'))
                        <div class="alert alert-success">
                        {{ session('status') }}
                        </div>

                       
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                    <form id="register-form" action="{{ URL('/verify_user') }}" method="post" role="form" style="display: none;">
                                    
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    
                                    <div class="form-group">
                                        <input type="text" name="otp" id="otp" class="form-control" placeholder="Enter OTP">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Verify">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- /#footer -->

	<!-- Template Javascript Files
	================================================== -->
	<!-- jquery -->
	<script src="{{asset('website_template/plugins/jQurey/jquery.min.js')}}"></script>
	<!-- Form Validation -->
    <script src="{{asset('website_template/plugins/form-validation/jquery.form.js')}}"></script> 
    <script src="{{asset('website_template/plugins/form-validation/jquery.validate.min.js')}}"></script>
	<!-- owl carouserl js -->
	<script src="{{asset('website_template/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
	<!-- bootstrap js -->
	<script src="{{asset('website_template/plugins/bootstrap/bootstrap.min.js')}}"></script>
	<!-- wow js -->
	<script src="{{asset('website_template/plugins/wow-js/wow.min.js')}}"></script>
	<!-- slider js -->
	<script src="{{asset('website_template/plugins/slider/slider.js')}}"></script>
	<!-- Fancybox -->
	<script src="{{asset('website_template/plugins/facncybox/jquery.fancybox.js')}}"></script>
	<!-- template main js -->
	<script src="{{asset('website_template/js/main.js')}}"></script>

    <script src="{{asset('website_template/plugins/jqnumscroller/numscroller-1.0.js')}}"></script>

    <script type="text/javascript">
            
      $(function() {

      $("#register-form").delay(100).fadeIn(100);
       e.preventDefault();

        

        });


    </script>

 	</body>
</html>