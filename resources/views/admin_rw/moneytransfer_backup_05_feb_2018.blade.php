@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')

    <h1><b>Money Transfer report</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css">
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
}

.card:hover {
    box-shadow: 0 0 50px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
    padding-bottom: 0px;
}
</style>


@section('content')

  <div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">
<!--           <h2> &nbsp; &nbsp;ABOUT US</h2>
 -->

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                
            
              <form action="{{URL('admin/view_moneytransfer')}}" method="get" style="margin: 10px auto">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="sdate"></label>
                    <input type="date" class='form-control' name='sdate' id='sdate'>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label for="edate"></label>
                    <input type="date" class='form-control' name='edate' id='edate'>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label for="edate"></label>
                    <button type="submit" class='btn btn-primary' name='getdetails' id='getdetails'>Get Details</button>
                  </div>
                </div>
              </form>

                        <div class="row box-body">
                            <div class="col-lg-12">
                                    
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th scope="col">Index</th>
                                      <th scope="col">Mobile Number</th>
                                      <th scope="col">Account number</th>
                                      <th scope="col">Recipient ID</th>
                                      <th scope="col">Channel</th>
                                      <th scope="col">Amount Transfered</th>
                                      <th scope="col">Fees</th>
                                      <th scope="col">Balance</th>
                                      <th scope="col">Collectable Amount</th>
                                      <th scope="col">Date</th>

                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($moneytransferdata as $money)
                                    <tr>
                                      <td>{{$money->id}}</td>
                                      <td>{{$money->customer_id}}</td>
                                      <td>{{$money->account_number}}</td>
                                      <td>{{$money->recipient_id}}</td>
                                      <td>{{$money->channel_desc}}</td>
                                      <td>{{$money->amount}}</td>
                                      <td>{{$money->fee}}</td>
                                      <td>{{$money->balance}}</td>
                                      <td>{{$money->collectable_amount}}</td>
                                      <td>{{$money->created_at}}</td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                </table>                     
                            
                                
                            </div>
                        </div>
          
        </div></div>


    

      

@stop