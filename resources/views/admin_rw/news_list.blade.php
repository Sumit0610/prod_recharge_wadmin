@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>News List</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

	<div class="container">
     <div class="box box-primary">


			<div class="box-body">
 				
				@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
 				

 			@if(!$news_data->isEmpty())
             @foreach($news_data as $key => $tem)
             <form action="{{ url('/admin/save_update_news') }}" enctype="multipart/form-data" method="post">
             <input type="hidden" name="_token" value="{!! csrf_token() !!}">
             <input type="hidden" name="newsid" id="newsid" value="{{ $tem->id }}">

             @if($tem->news_desc!='' || $tem->news_desc!=null || $tem->news_desc!=NULL)
             <div class="col-md-12">
                <div class="form-group">
                  <label for="newsdesc" class="col-lg-5 control-label"><dt>Latest News</dt></label>
					<textarea class="form-control" id="newsdesc" placeholder="Enter News description" name="newsdesc" rows="4">{{$tem->news_desc}}</textarea>
                </div>
                
             </div>
            
             @endif

             <br>
             <div class="col-md-8">
	            <div class="form-group">
	              <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>Update data</button>
	            </div><br>
             </div>

             </form>
             @endforeach
             @endif

	          

 			</div>

 			

 		

     </div>
    </div>

@stop