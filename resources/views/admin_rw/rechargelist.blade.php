@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Recharge report</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif
        

      @foreach ($rechargedata as $key => $value) 

      <a href="{{ url('/admin/view_rechargelist/'.$value->recreportid) }}">

      <div class="card">
        <div class="container">

           @if(isset($value->name))
          <div class="col-md-9">
            <div class="form-group">
              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Agent Name </dt></label>
          
              <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="{{$value->name}} - {{$value->user_phone}}">
            </div>

           </div>
        @endif

       
          <div class="col-md-9">
            <div class="form-group">
              <label for="aboutus_desc" class="col-lg-5 control-label"><dt> Amount & Status </dt></label>
          
              <input type="text" readonly class="form-control" id="servicename" placeholder="Operator Name" name="servicename" value="₹{{$value->recharge_amount}} - {{$value->recharge_done}}">
            </div>

           </div>

             </div></div>      

             </a>

         @endforeach     

            

      </div>

      

    

     </div>
    </div>

@stop