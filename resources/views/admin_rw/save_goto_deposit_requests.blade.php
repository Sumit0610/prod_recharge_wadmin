@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Deposit Requests</b></h1>
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@section('content')

<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: auto;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
    top: 50%;
}

.wrapperss {
    text-align: center;
    background: white;
    margin-bottom: 30px;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}

#documents_needed {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#documents_needed td, #documents_needed th {
    border: 1px solid #ddd;
    padding: 4px;
    font-size: 12px;
}

#documents_needed tr:nth-child(even){background-color: #f2f2f2;}

#documents_needed tr:hover {background-color: #ddd;}

#documents_needed th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #00A65A;
    color: white;
}

</style>

  <div class="container">
     <div class="box box-primary">

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif
      @if(session('error'))
        <div class="alert alert-danger">
        {{ session('error') }}
        </div>
      @endif


            <div class="row box-body">
                    <div class="col-lg-10">

                            <input id="make_text" type = "hidden" name = "make_text" value = "" />

                            

                            <div class="form-group col-md-5">
                                <label for="payment_date"> Date of deposit </label>
                                <input type="date" name="payment_date" id="payment_date" tabindex="1" class="form-control" value="{{ $deposit_requests->date_of_deposit }}" readonly>
                            </div>
                            <div class="form-group col-md-8">
                            <label for="deposit_type">Deposit Type</label>
                             @if($deposit_requests->deposit_type == 0)
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="Cheque" readonly>
                             @elseif($deposit_requests->deposit_type == 1)  
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="Cash" readonly>
                             @elseif($deposit_requests->deposit_type == 2) 
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="NEFT/RTGS" readonly>
                             @elseif($deposit_requests->deposit_type == 3)  
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="ATM Transfer" readonly>
                             @elseif($deposit_requests->deposit_type == 4)  
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="Credit" readonly>
                             @elseif($deposit_requests->deposit_type == 5)  
                                <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="Refund" readonly>
                             @endif   
                            </div> 


                            @if($deposit_requests->deposit_type == 0)
                                <div id="payment_cheque" >     

                                <div class="form-group col-md-8" id="divcheque">
                                    <label for="issuance_date"> Cheque/DD No. </label>
                                   <input type="text" name="cheque_no" id="cheque_no" tabindex="1" class="form-control" placeholder="Cheque/DD No" value="{{ $deposit_requests->cheque_no }}" readonly>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group col-md-8" id="divbankacc">
                                    <label for="issuance_date"> Your Bank Account No </label>
                                    <input type="text" name="bankaccno" id="bankaccno" tabindex="1" class="form-control" placeholder="Your Bank Account No" value="{{ $deposit_requests->bankaccno }}" readonly>
                                </div>

                                <div class="form-group col-md-8" id="divbankname">
                                     <label for="issuance_date"> Your Bank Name </label>
                                   <input type="text" name="bankname" id="bankname" tabindex="1" class="form-control" placeholder="Your Bank Name" value="{{ $deposit_requests->bankname }}" readonly>
                                </div>

                                <div class="form-group col-md-8" id="divbankbranch">
                                   <label for="issuance_date"> Your bank branch </label>
                                    <input type="text" name="bankbranch" id="bankbranch" tabindex="1" class="form-control" placeholder="Your Bank Branch Name" value="{{ $deposit_requests->bankbranch }}" readonly>
                                </div>

                               <div class="form-group col-md-5">
                                    <label for="issuance_date"> Date of issuance </label>
                                    <input type="date" name="issuance_date" id="issuance_date" tabindex="1" class="form-control" value="{{ $deposit_requests->issuance_date }}" readonly>
                                </div>

                                </div>

                            @elseif($deposit_requests->deposit_type == 1)   
                            <div id="payment_cash" >     

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Benificiary Bank </label>
                               <input type="text" name="benificiary_bank" id="benificiary_bank" tabindex="1" class="form-control" placeholder="Benificiary Bank" value="{{ $deposit_requests->bankname }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Benificiary Bank Branch </label>
                                <input type="text" name="benificiary_bankbranch" id="benificiary_bankbranch" tabindex="1" class="form-control" placeholder="Benificiary Bank Branch" value="{{ $deposit_requests->bankbranch }}" readonly>
                            </div>

                            </div>


                            @elseif($deposit_requests->deposit_type == 2)   
                           
                            <div id="payment_neft_rtgs" >     

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Benificiary Bank Name </label>
                               <input type="text" name="benificiary_bank_name" id="benificiary_bank_name" tabindex="1" class="form-control" placeholder="Benificiary Bank Name" value="{{ $deposit_requests->bankname }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Your Bank Name </label>
                                <input type="text" name="bank_name" id="bank_name" tabindex="1" class="form-control" placeholder="Your Bank Name" value="{{ $deposit_requests->bank_name }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Account Holder Name </label>
                                <input type="text" name="accountholder_name" id="accountholder_name" tabindex="1" class="form-control" placeholder="Account Holder Name" value="{{ $deposit_requests->accountholder_name }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Transaction ID </label>
                                <input type="text" name="transaction_id" id="transaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="{{ $deposit_requests->transaction_id }}" readonly>
                            </div>

                            </div>


                            @elseif($deposit_requests->deposit_type == 3)   

                            <div id="payment_atm_transfer" >     

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Benificiary Bank Name </label>
                               <input type="text" name="atmbenificiary_bank_name" id="atmbenificiary_bank_name" tabindex="1" class="form-control" placeholder="Benificiary Bank Name" value="{{ $deposit_requests->bankname }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Transaction ID </label>
                                <input type="text" name="atmtransaction_id" id="atmtransaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="{{ $deposit_requests->transaction_id }}"  readonly>
                            </div>

                            </div>


                            @elseif($deposit_requests->deposit_type == 4)   

                            <div id="payment_credit" >     

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Reference Name </label>
                               <input type="text" name="reference_name" id="reference_name" tabindex="1" class="form-control" placeholder="Reference Name" value="{{ $deposit_requests->reference_name }}" readonly>
                            </div>

                            <div class="form-group col-md-8">
                                <label for="issuance_date"> Transaction ID </label>
                                <input type="text" name="credittransaction_id" id="credittransaction_id" tabindex="1" class="form-control" placeholder="Transaction ID" value="{{ $deposit_requests->transaction_id }}" readonly>
                            </div>

                            </div>

                            @endif

                            @if(isset($deposit_requests->remarks))
                            <div class="form-group col-md-8">
                                <label for="remarks"> Remarks </label>
                                <input type="text" name="remarks" id="remarks" tabindex="1" class="form-control" placeholder="Remarks" value="{{ $deposit_requests->remarks }}" readonly>
                            </div>
                            @endif

                            @if(!isset($deposit_requests->status))

                            <div class="form-group col-md-8">

                            <div class="col-md-6">

                            <form id="register-form" action="{{ URL('admin/save_update_dep_requests') }}" method="post" role="form">

                            <input id="trid" type = "hidden" name="trid" value = "{{ $deposit_requests->id }}" />

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input type="hidden" name="is_button" value="1">
                          
                                <div class="form-group">
                                 <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="ACCEPT" style="background: #00B9F5; color: white;">
                                 </div>
                                </div>

                            </form>

                            </div>


                            <div class=" col-md-6">
                            <form id="register-form" action="{{ URL('admin/save_update_dep_requests') }}" method="post" role="form">

                            <input id="trid" type = "hidden" name="trid" value = "{{ $deposit_requests->id }}" />

                            <input type="hidden" name="is_button" value="2">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                 <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="REJECT" style="background: red; color: white;">
                                 </div>
                                </div>

                            </form>
                            </div>

                            </div>
                            @endif
                        
                    </div>
                </div>

                    
      </div>

     </div>
  </div>

@stop