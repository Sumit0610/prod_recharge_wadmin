@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>About Us details</b></h1>
@stop

@section('content')


	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp;About Details List</h2>
 -->


          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif


			<div class="box-body">
 				
 			

             @if(!$service_data->isEmpty())
             @foreach($service_data as $key => $tem)
             <form action="{{ url('/admin/service_update') }}" enctype="multipart/form-data" method="post">
             <input type="hidden" name="_token" value="{!! csrf_token() !!}">
             <input type="hidden" name="id" value="{{ $tem->id }}">

             @if($tem->service_name!='' || $tem->service_name!=null || $tem->service_name!=NULL)
             <div class="col-md-8">
	            <div class="form-group">
	              <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Service Name</dt></label>
	          
	              <input type="text" class="form-control" id="teammemnm" placeholder="Enter Service Name" name="teammemnm" value="{{$tem->service_name}}">
	            </div>

             </div>
            
             @endif

             @if($tem->service_desc!='' || $tem->service_desc!=null || $tem->service_desc!=NULL)
             <div class="col-md-8">
	            <div class="form-group">
	              <label for="aboutus_desc" class="col-lg-5 control-label"><dt>Service short description</dt></label>
	          
	              <input type="text" class="form-control" id="teammempos" placeholder="Enter service short description" name="teammempos" value="{{$tem->service_desc}}">
	            </div>

             </div>
             @endif


             @if($tem->service_desc_detail!='' || $tem->service_desc_detail!=null || $tem->service_desc_detail!=NULL)
             <div class="col-md-8">
	            <div class="form-group">
	              <label for="service_desc_detail" class="col-lg-5 control-label"><dt>Service brief description</dt></label>
	          
	              <input type="text" class="form-control" id="service_desc_detail" placeholder="Enter service description in brief" name="service_desc_detail" value="{{$tem->service_desc_detail}}">
	            </div>

             </div>
             @endif

             <br>
             <div class="col-md-8">
	            <div class="form-group">
	              <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>Update data</button>
	            </div><br>
             </div>

             
             <div class="col-md-8">
	            <div class="form-group">
	              
	          	
<!-- 	          	<input type="file" name="temimg" class="form-control">
 -->	          	
  @if($tem->service_img!='' || $tem->service_img!=null || $tem->service_img!=NULL)
	          	<img src="{{asset('uploads/services/')}}/{{$tem->service_img}}"width="200"><br><br>

	          	<a href="{{ url('/admin/edit_service_photo/?id=' . $tem->id) }}" style=" display: block;
    background: #00c0ef;
    padding: 10px;
    width: 200px;
    text-align: center;
    border-radius: 5px;
    color: white;
    margin-bottom: 25px;
    "> Update photo </a>

	          	 @endif

	          	 <br>

			
 			
 			
	              
	            </div>

             </div>
             

            </form>
             
             @endforeach

             @endif


              

     </div>
    </div>



@stop