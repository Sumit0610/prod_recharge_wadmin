@extends('adminlte::page')

@section('title', 'Admin LTE')

@section('content_header')
    <h1><b>Add DTH provider</b></h1>
@stop    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@section('content')

  <div class="container">
     <div class="box box-primary" style="padding: bottom: 0px;">

            @if(session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                {{ session('error') }}
                </div>
            @endif                

                        <div class="row box-body">
                            <div class="col-lg-10">
                                    <form id="register-form" action="{{ URL('admin/save_set_dth_commission') }}" method="post" role="form">
                                    <div class="form-group">
                                        <input type="text" name="opname" id="opname" tabindex="1" class="form-control" placeholder="Operator Name" required>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <input type="number" name="rate" id="rate" tabindex="1" class="form-control" placeholder="Rate" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="commission" id="commission" tabindex="1" class="form-control" placeholder="Commission" value="0" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Amount" readonly>
                                    </div>
                                   
                                    <div class="form-group">
                                    <div class="form-group float-label-control">
                                    <input type="submit" name="register-submit" class="form-control" value="Add DTH Operator" style="background: #00B9F5; color: white;">
                                    </div></div>
                                </form>
                                
                            </div>
                        </div>
          
        </div></div> 


        <script type="text/javascript">

        $(document).ready(function() {
        //this calculates values automatically 
            sum();
            $("#commission, #rate").on("keydown keyup", function() {
            sum();
         });
        });

        function sum() {
            var num1 = document.getElementById('commission').value;
            var num2 = document.getElementById('rate').value;
            console.log(num1);
            console.log(num2);
            var result = parseInt(num1) + parseInt(num2);
            var result1 = parseInt(num2) - parseInt(num1);
            if (!isNaN(result)) {
                document.getElementById('amount').value = result1;
            }
            console.log(result1);
        }

        </script>

@stop