@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>Unverified Users List</b></h1>
@stop

@section('content')

<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    padding-top: 20px;
}
</style>

  <div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp; ServiceList</h2>
 -->

      <div class="box-body">
        
      @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
      @if(session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
      @endif
        

      @foreach ($unverified_users as $key => $value) 

      <div class="card">
        <div class="container">     

        <form action="{{ url('/admin/verify_apguser') }}" method="post">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="userid" value="{{ $value->id }}">        
          
           @if(isset($value->name))
        <div class="col-md-8">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Name </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicename" placeholder="Name" name="servicename" value="{{$value->name}}">
                </div>

               </div>
            @endif

            @if(isset($value->email))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt> User Email </dt></label>
              
                  <input type="text" readonly class="form-control" id="servicedesc" placeholder="User Email" name="servicedesc" value="{{$value->email}}">
                </div>

               </div>
             @endif

             @if(isset($value->user_phone))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> User Contact </dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="User Subject" name="subject_message" value="{{$value->user_phone}}">
                </div>

               </div>


             @endif 

             @if(isset($value->user_type))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="aboutus_desc" class="col-lg-5 control-label"><dt>User Type</dt></label>
                  
                  @if($value->user_type == '0')
                    <input type="text" readonly class="form-control" id="servicedesc" placeholder="User Message" name="servicedesc" value="Master Distributor">

                  @elseif($value->user_type == '1')
                    <input type="text" readonly class="form-control" id="servicedesc" placeholder="User Message" name="servicedesc" value="Distributor">

                  @elseif($value->user_type == '2')
                   <input type="text" readonly class="form-control" id="servicedesc" placeholder="User Message" name="servicedesc" value="Retailer">

                  @endif
                </div>
               </div>

             @endif 

             @if(isset($value->panno))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt>User PAN No</dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="User Subject" name="subject_message" value="{{$value->panno}}">
                </div>

               </div>


             @endif 

             @if(isset($value->address))
               <div class="col-md-8">
                <div class="form-group">
                  <label for="subject_message" class="col-lg-5 control-label"><dt> User Address </dt></label>
              
                  <input type="text" readonly class="form-control" id="subject_message" placeholder="User Subject" name="subject_message" value="{{$value->address}}">
                </div>

               </div>

             @endif 

              <div class="form-group col-md-8">
              <div class="form-group float-label-control col-md-8">
              <input type="submit" name="register-submit" class="form-control" value="Verify User" style="background: #00B9F5; color: white;"> </form>
              </div></div>



             </div></div>      

         @endforeach     

            

      </div>

      

    

     </div>
    </div>

@stop