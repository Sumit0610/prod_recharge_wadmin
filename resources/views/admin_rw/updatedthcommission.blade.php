@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><b>DTH Operators</b></h1>
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@section('content')


	<div class="container">
     <div class="box box-primary">
<!--           <h2> &nbsp; &nbsp;About Details List</h2>
 -->


          
		  	@foreach ($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
			@endforeach
			@if(session('status'))
				<div class="alert alert-success">
				{{ session('status') }}
				</div>
			@endif
			@if(session('error'))
				<div class="alert alert-danger">
				{{ session('error') }}
				</div>
			@endif

			<div class="box-body"> 				

             @if(!$dthdata->isEmpty())
             @foreach($dthdata as $key => $tem)
             <form action="{{ url('/admin/save_goto_dth_commission') }}" enctype="multipart/form-data" method="post">
             <input type="hidden" name="_token" value="{!! csrf_token() !!}">
             <input type="hidden" name="id" value="{{ $tem->id }}">

             @if($tem->operator_name!='' || $tem->operator_name!=null || $tem->operator_name!=NULL)
             <div class="col-md-8">
	            <div class="form-group">
	              <label for="operator_name" class="col-lg-5 control-label"><dt>Operator Name</dt></label>
	              <input type="text" class="form-control" id="operator_name{{ $tem->id }}" placeholder="Enter Operator Name" name="operator_name{{ $tem->id }}" value="{{$tem->operator_name}}" required>
	            </div>

             </div>
            
             @endif

             <br>
             <div class="col-md-8">
	            <div class="form-group">
	              <button type="submit" class="btn btn-info" name="sub1"><i class="fa fa-save"></i>Proceed to update</button>
	            </div><br>
             </div>           

            </form>
             
             @endforeach

             @endif


              

     </div>
    </div>

<script type="text/javascript">

        function sum() {
           
            console.log($(this).attr('id'));
        }

        </script>

@stop