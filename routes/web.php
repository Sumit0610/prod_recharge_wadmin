<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', function(){
	return view('admin_rw.home');
});	

Route::get('admin/logo_add', 'UserController@logo_add');
Route::post('admin/logo_add', 'UserController@save_logo_add');

Route::get('admin/about_add', 'UserController@about_add');
Route::post('admin/save_about_add', 'UserController@save_about_add');

Route::get('admin/update_about', 'UserController@update_about');
Route::post('admin/save_update_about', 'UserController@save_update_about');

Route::get('admin/service_add', 'UserController@service_add');
Route::post('admin/service_add', 'UserController@save_service_add');

Route::get('admin/service_update', 'UserController@service_update');
Route::post('admin/service_update', 'UserController@save_service_update');

Route::get('admin/edit_service_photo', 'UserController@edit_service_photo');
Route::post('admin/edit_service_photo', 'UserController@edit_service_photo_save');

Route::get('admin/service_delete', 'UserController@service_delete');
Route::post('admin/service_delete', 'UserController@save_service_delete');

Route::get('admin/view_contacts', 'UserController@view_contacts');

Route::get('admin/new_old_user', 'UserController@new_old_user');

Route::post('/save_website_user', 'UserController@save_website_user');

Route::post('/verify_user', 'UserController@verify_user');

Route::get('/enter_otp', 'UserController@enter_otp');

Route::get('/admin/verify_users', 'UserController@verify_users');

Route::get('/admin/view_users', 'UserController@view_users');

Route::post('/admin/verify_apguser', 'UserController@verify_apguser');

Route::get('/admin/add_updates', 'UserController@add_updates');

Route::post('/admin/save_updates', 'UserController@save_updates');

Route::get('/admin/update_news', 'UserController@update_news');

Route::post('/admin/save_update_news', 'UserController@save_update_news');

Route::get('/admin/delete_news', 'UserController@delete_news');

Route::post('/admin/save_delete_news', 'UserController@save_delete_news');

Route::get('admin/add_dth_operators', 'UserController@set_dth_commission');

Route::post('admin/save_set_dth_commission', 'UserController@save_set_dth_commission');

Route::get('admin/update_dth_operators', 'UserController@set_update_dth_commission');

Route::post('admin/save_update_dth_commission', 'UserController@save_update_dth_commission');

Route::post('admin/save_goto_dth_commission', 'UserController@savegoto');

Route::get('admin/goto_dthupdate/{q}', 'UserController@goto_dthupdate');

Route::get('admin/deposit_requests', 'UserController@deposit_requests');

Route::post('admin/save_goto_deposit_requests', 'UserController@save_goto_deposit_requests');

Route::get('admin/update_dep_requests/{q}', 'UserController@update_dep_requests');

Route::post('admin/save_update_dep_requests', 'UserController@save_update_dep_requests');

Route::get('admin/accept_reject_history', 'UserController@wallet_history');


Route::get('admin/add_to_wallet', 'UserController@add_to_wallet');

Route::post('admin/save_add_to_wallet', 'UserController@save_add_to_wallet');

Route::get('admin/wallet_history', 'UserController@wallethistory');

Route::get('admin/view_dth_sales', 'UserController@view_dth_sales');

Route::get('admin/view_recharge', 'UserController@view_recharge');

Route::get('admin/view_moneytransfer', 'UserController@view_moneytransfer');

Route::get('admin/view_pancardreport', 'UserController@view_pancardreport');

Route::get('admin/dthsaleshist/{q}', 'UserController@dthsaleshist');

Route::get('admin/view_rechargelist/{q}', 'UserController@view_rechargelist');


Route::get('admin/create_scheme', 'UserController@create_scheme');
Route::post('admin/create_scheme', 'UserController@save_scheme');
Route::get('admin/view_schemes', 'UserController@view_schemes');
Route::get('admin/updatescheme/{id}', 'UserController@updatescheme');
Route::post('admin/updatescheme', 'UserController@saveupdatedscheme');
Route::get('admin/deletescheme/{id}', 'UserController@deletescheme');

Route::get('admin/add_states', 'UserController@add_states');
Route::get('admin/update_states', 'UserController@update_states');
Route::get('admin/delete_states', 'UserController@delete_states');

Route::post('admin/save_add_states', 'UserController@save_add_states');
Route::post('admin/save_update_states', 'UserController@save_update_states');
Route::post('admin/save_delete_states', 'UserController@save_delete_states');
